#include <iostream>;
#include <string>;
using namespace std;

string start;
int l,a,b,amax,bmax,pmax,p1,p2;

int main() {
	
	cout <<"Welcome on McDonald's farm!\nThank you for bringin the fence, type RUN if you want to help us design the area for animals." << endl;
	cin >> start;
	
			if (start == "RUN") {
				cout<<"Great! How long is the fence You bring? (only integer values, greater or equal 3)"<<endl;
				cin >> l;
				
				if(l>=3){
					
					for (a=1; b<l; a++){
						p2=p1;
						b=l-(2*a);
						p1=a*b;
						
						if(p2>p1){
						amax=a-1;
						bmax=b+2;
						pmax=p2;
						break;
						}
					}
					
				cout << "For " << l << " lenght fence, optimum solution is first side " << amax << " lenght and second side " << bmax << " lenght." <<endl;
				cout << "   ||\n   ||\n   ||\t a = " << amax <<endl;
				cout << "   ||____________\n   ||           |\n  W||           |\n  A||           |\tb = " << bmax << "\n  L||           |\n  L||           |" << endl;
				cout << "   ||___________|\n   ||\n   ||\n   ||" << endl;
				cout << "The optimum area for these dimensions equal " << pmax <<endl;
				}
				
				else {
					cout <<"Ooops! Typed value is less than 3. Restart the app." <<endl;
				}
				
			}
			
			else {
				cout <<"Ooops! You typed the wrong command. Restart the app." <<endl;
			}
			
	return 0;
}
